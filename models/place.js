const mongoose = require("mongoose");

const Place = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  address: {
    type: String,
    required: true,
  },
  imageUrl: {
    type: String,
    required: true,
  },
  allImages: {
    type: [String],
    required: false,
  },
  dictionary: {
    type: [String],
    required: false,
  },
});

module.exports = mongoose.model("Place", Place);
