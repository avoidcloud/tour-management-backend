const mongoose = require("mongoose");

const Hotel = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  address: {
    type: String,
    required: true,
  },
  imageUrl: {
    type: String,
    required: true,
  },
  bookingPrice: {
    type: Number,
    required: true,
  },
  allImages: {
    type: [String],
    required: false,
  },
  features: {
    type: [String],
    required: false,
  },
});

module.exports = mongoose.model("Hotel", Hotel);
