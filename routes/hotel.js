const express = require("express");
const { authentication, admin } = require("../middlewares/auth");
const Hotel = require("../models/Hotel");

const hotelRoutes = express.Router();

const getAllHotels = async (req, res, next) => {
  try {
    const hotels = await Hotel.find();
    res.json(hotels);
  } catch (err) {
    res.send(err);
  }
};

const createHotel = async (req, res, next) => {
  const hotel = new Hotel({
    name: req.body.name,
    address: req.body.address,
    description: req.body.description,
    imageUrl: req.body.imageUrl,
    bookingPrice: req.body.bookingPrice,
  });
  try {
    const result = await hotel.save();
    res.json(hotel);
  } catch (err) {
    res.json(err);
  }
};

const getHotelById = async (req, res, next) => {
  try {
    const hotel = await Hotel.findById(req.params.id);
    res.json(hotel);
  } catch (err) {
    res.send("Error " + err);
  }
};

const updateHotel = async (req, res, next) => {
  try {
    const hotel = await Hotel.findById(req.params.id);
    hotel.name = req.body.name;
    hotel.description = req.body.description;
    hotel.address = req.body.address;
    hotel.imageUrl = req.body.imageUrl;
    hotel.bookingPrice = req.body.bookingPrice;
    await hotel.save();
    res.json(hotel);
  } catch (err) {
    res.send("Error !!" + err);
  }
};

const deleteHotel = async (req, res, next) => {
  try {
    const hotel = await Hotel.findById(req.params.id);
    await hotel.delete();
    res.json();
  } catch (err) {
    res.send("Error !!" + err);
  }
};

hotelRoutes.get("/", getAllHotels);
hotelRoutes.post("/", authentication, admin, createHotel);
hotelRoutes.get("/:id", getHotelById);
hotelRoutes.put("/:id", authentication, admin, updateHotel);
hotelRoutes.delete("/:id", authentication, admin, deleteHotel);

module.exports = hotelRoutes;
