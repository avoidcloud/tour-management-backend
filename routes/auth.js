const express = require("express");
const { generateAuthToken } = require("../middlewares/auth");
const User = require("../models/user");

const authRoutes = express.Router();

const handleLogin = async (req, res, next) => {
  try {
    const user = await User.findOne({
      email: req.body.email,
      password: req.body.password,
    });
    if (user === null) {
      res.status(401).json({ message: "unathorized" }).end();
      return;
    }
    const token = generateAuthToken(user);
    res.send({ bearer: token });
  } catch (err) {
    res.status(401);
    res.json({ message: err });
  }
};

authRoutes.post("/login", handleLogin);

module.exports = authRoutes;
