const express = require("express");
const { authentication } = require("../middlewares/auth");

const profileRoutes = express.Router();

const getUserProfile = (req, res, next) => {
  res.send(req.user);
};

profileRoutes.get("/", authentication, getUserProfile);

module.exports = profileRoutes;
