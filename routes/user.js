const express = require("express");
const { authentication, admin } = require("../middlewares/auth");
const User = require("../models/user");
const { Role } = require("../util");

const userRoutes = express.Router();

const handleGetAllUsers = async (req, res, next) => {
  try {
    const users = await User.find();
    res.json(users);
  } catch (err) {
    res.send(err);
  }
};

const handleUserCreate = async (req, res, next) => {
  const user = new User({
    name: req.body.name,
    password: req.body.password,
    gender: req.body.gender,
    email: req.body.email,
    role: Role.User,
  });
  try {
    await user.save();
    res.json(user);
  } catch (err) {
    res.json(err);
  }
};

const handleGetUserById = async (req, res, next) => {
  if (req.user.id !== req.params.id) {
    res.status(403).send({ message: "access denied" });
    return;
  }
  try {
    const user = await User.findById(req.params.id);
    res.json(user);
  } catch (err) {
    res.send("Error " + err);
  }
};

const handleUserUpdate = async (req, res, next) => {
  if (req.user.id !== req.params.id) {
    res.status(403).send({ message: "access denied" });
    return;
  }
  try {
    const user = await User.findById(req.params.id);
    user.name = req.body.name;
    user.gender = req.body.gender;
    const updatedUser = await user.save();
    res.json(updatedUser);
  } catch (err) {
    res.send("Error !!" + err);
  }
};

const handleUserDelete = async (req, res, next) => {
  if (req.user.id !== req.params.id) {
    res.status(403).send({ message: "access denied" });
    return;
  }
  try {
    const user = await User.findById(req.params.id);
    await user.delete();
    res.json();
  } catch (err) {
    res.send("Error !!" + err);
  }
};

userRoutes.get("/", authentication, admin, handleGetAllUsers);
userRoutes.post("/", handleUserCreate);
userRoutes.get("/:id", authentication, handleGetUserById);
userRoutes.put("/:id", authentication, handleUserUpdate);
userRoutes.delete("/:id", authentication, handleUserDelete);

module.exports = userRoutes;
