const express = require("express");
const { authentication, admin } = require("../middlewares/auth");
const Place = require("../models/Place");

const placeRoutes = express.Router();

const getAllPlaces = async (req, res, next) => {
  try {
    const places = await Place.find();
    res.json(places);
  } catch (err) {
    res.send(err);
  }
};

const createPlace = async (req, res, next) => {
  const place = new Place({
    name: req.body.name,
    address: req.body.address,
    description: req.body.description,
    imageUrl: req.body.imageUrl,
  });
  try {
    const result = await place.save();
    res.json(place);
  } catch (err) {
    res.json(err);
  }
};

const getPlaceById = async (req, res, next) => {
  try {
    const place = await Place.findById(req.params.id);
    res.json(place);
  } catch (err) {
    res.send("Error " + err);
  }
};

const updatePlace = async (req, res, next) => {
  try {
    const place = await Place.findById(req.params.id);
    place.name = req.body.name;
    place.description = req.body.description;
    place.address = req.body.address;
    place.imageUrl = req.body.imageUrl;
    const updatedPlace = await place.save();
    res.json(updatedPlace);
  } catch (err) {
    res.send("Error !!" + err);
  }
};

const deletePlace = async (req, res, next) => {
  try {
    const place = await Place.findById(req.params.id);
    await place.delete();
    res.json();
  } catch (err) {
    res.send("Error !!" + err);
  }
};

placeRoutes.get("/", getAllPlaces);
placeRoutes.post("/", authentication, admin, createPlace);
placeRoutes.get("/:id", getPlaceById);
placeRoutes.put("/:id", authentication, admin, updatePlace);
placeRoutes.delete("/:id", authentication, admin, deletePlace);

module.exports = placeRoutes;
