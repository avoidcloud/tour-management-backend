const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();
const { seed } = require("./setup");

const url = "mongodb://localhost/TourManagement";

app.use(express.json());
app.use(cors());

mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
const con = mongoose.connection;

con.on("open", () => {
  console.log("connected.......!!");
});

// seed initial entries: default admin account, default hotels, default places
// run only one time
// seed();

const userRoutes = require("./routes/user");
const placeRoutes = require("./routes/place");
const hotelRoutes = require("./routes/hotel");
const authRoutes = require("./routes/auth");
const profileRoutes = require("./routes/profile");

app.use("/users", userRoutes);
app.use("/places", placeRoutes);
app.use("/hotels", hotelRoutes);
app.use("/auth", authRoutes);
app.use("/profile", profileRoutes);

const port = process.env.PORT || 9000;

app.listen(port, () => console.log(`Server running at port ${port}`));
