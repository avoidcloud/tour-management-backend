const Hotel = require("./models/Hotel");
const Place = require("./models/Place");
const User = require("./models/user");
const { Role } = require("./util");

const seedDefaultAdmin = async () => {
  const admin = new User({
    name: "admin",
    password: "admin1234",
    gender: "N/A",
    email: "admin@admin.com",
    role: Role.Admin,
  });
  console.log("[INF] creating default admin account");
  try {
    await admin.save();
    console.log("[INF] admin account created successfully!");
  } catch (err) {
    console.error("[ERR] failed to create admin account " + err);
  }
};

const seedDefaultPlaces = async () => {
  const place1 = new Place({
    name: "Place One",
    address: "Dhaka, Bangladesh",
    description: `
    Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
    when an unknown printer took a galley of type and scrambled it to make a type specimen book.
    It has survived not only five centuries,but also the leap into electronic typesetting, remaining essentially unchanged. 
    It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, 
    and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
    imageUrl:
      "https://nijhoom.b-cdn.net/wp-content/uploads/2021/03/lalbagh-fort-old-dhaka-tomb-768-o.jpg",
    dictionary: [
      "Name : Place One",
      "Population : 19999",
      "Area : 12 SqKm",
      "Key1 : Value1",
      "Key2 : Value2",
      "Key3 : Value3",
      "Key4 : Value4",
      "Key5 : Value5",
      "Key6 : Value6",
    ],
    allImages: [
      "https://blog.flyticket.com.bd/wp-content/uploads/2020/05/image003.jpg",
      "https://www.worldatlas.com/r/w960-q80/upload/6f/99/8f/shutterstock-114479500.jpg",
      "https://www.worldatlas.com/r/w960-q80/upload/3d/bd/0a/shutterstock-583939735.jpg",
      "https://www.worldatlas.com/r/w960-q80/upload/5c/36/ae/shutterstock-1014644104.jpg",
      "https://www.worldatlas.com/r/w960-q80/upload/7a/93/ae/shutterstock-1174014040.jpg",
      "https://www.worldatlas.com/r/w960-q80/upload/ae/7e/c4/shutterstock-557889964.jpg",
    ],
  });
  const place2 = new Place({
    name: "Place Tow",
    address: "Sylhet, Bangladesh",
    description: `
    Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
    when an unknown printer took a galley of type and scrambled it to make a type specimen book.
    It has survived not only five centuries,but also the leap into electronic typesetting, remaining essentially unchanged. 
    It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, 
    and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
    imageUrl:
      "https://www.planetware.com/wpimages/2019/09/top-places-to-visit-in-the-world-paris-france.jpg",
    dictionary: [
      "Name : Place One",
      "Population : 19999",
      "Area : 12 SqKm",
      "Key1 : Value1",
      "Key2 : Value2",
      "Key3 : Value3",
      "Key4 : Value4",
      "Key5 : Value5",
      "Key6 : Value6",
    ],
    allImages: [
      "https://blog.flyticket.com.bd/wp-content/uploads/2020/05/image003.jpg",
      "https://www.worldatlas.com/r/w960-q80/upload/6f/99/8f/shutterstock-114479500.jpg",
      "https://www.worldatlas.com/r/w960-q80/upload/3d/bd/0a/shutterstock-583939735.jpg",
      "https://www.worldatlas.com/r/w960-q80/upload/5c/36/ae/shutterstock-1014644104.jpg",
      "https://www.worldatlas.com/r/w960-q80/upload/7a/93/ae/shutterstock-1174014040.jpg",
      "https://www.worldatlas.com/r/w960-q80/upload/ae/7e/c4/shutterstock-557889964.jpg",
    ],
  });
  const place3 = new Place({
    name: "Place One",
    address: "Khulna, Bangladesh",
    description: `
    Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
    when an unknown printer took a galley of type and scrambled it to make a type specimen book.
    It has survived not only five centuries,but also the leap into electronic typesetting, remaining essentially unchanged. 
    It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, 
    and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
    imageUrl: "https://ihplb.b-cdn.net/wp-content/uploads/2014/03/Kashmir1.jpg",
    dictionary: [
      "Name : Place One",
      "Population : 19999",
      "Area : 12 SqKm",
      "Key1 : Value1",
      "Key2 : Value2",
      "Key3 : Value3",
      "Key4 : Value4",
      "Key5 : Value5",
      "Key6 : Value6",
    ],
    allImages: [
      "https://blog.flyticket.com.bd/wp-content/uploads/2020/05/image003.jpg",
      "https://www.worldatlas.com/r/w960-q80/upload/6f/99/8f/shutterstock-114479500.jpg",
      "https://www.worldatlas.com/r/w960-q80/upload/3d/bd/0a/shutterstock-583939735.jpg",
      "https://www.worldatlas.com/r/w960-q80/upload/5c/36/ae/shutterstock-1014644104.jpg",
      "https://www.worldatlas.com/r/w960-q80/upload/7a/93/ae/shutterstock-1174014040.jpg",
      "https://www.worldatlas.com/r/w960-q80/upload/ae/7e/c4/shutterstock-557889964.jpg",
    ],
  });

  console.log("[INF] creating places");
  try {
    await place1.save();
    await place2.save();
    await place3.save();
    console.log("[INF] places created successfully!");
  } catch (err) {
    console.error("[ERR] failed to create places " + err);
  }
};

const seedDefaultHotels = async () => {
  const hotel1 = new Hotel({
    name: "Hyatt",
    address: "Dhaka, Bangladesh",
    description: `
     Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
     Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,`,
    imageUrl:
      "https://media-cdn.tripadvisor.com/media/photo-s/1c/3f/f3/1e/hrmdor.jpg",
    bookingPrice: 5000,
    allImages: [
      "https://images.pexels.com/photos/164595/pexels-photo-164595.jpeg",
      "https://pix10.agoda.net/hotelImages/124/1246280/1246280_16061017110043391702.jpg",
      "https://cdn.britannica.com/96/115096-050-5AFDAF5D/Bellagio-Hotel-Casino-Las-Vegas.jpg",
      "https://bsmedia.business-standard.com/_media/bs/img/article/2021-02/05/full/1612508290-954.jpg",
      "https://www.epichotel.com/images/1700-960/one-bedroom-suite-05-c1d982d5.jpg",
    ],
    features: [
      "WiFi",
      "Swimming Pool",
      "Game Zone",
      "Movie Theatre",
      "24/7 Support",
    ],
  });

  const hotel2 = new Hotel({
    name: "Pan Pacific",
    address: "Dhaka, Bangladesh",
    description: `
     Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
     Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,`,
    imageUrl:
      "https://pix8.agoda.net/hotelImages/276/276118/276118_16031014010040651645.jpg?s=1024x768",
    bookingPrice: 3400,
    allImages: [
      "https://images.pexels.com/photos/164595/pexels-photo-164595.jpeg",
      "https://pix10.agoda.net/hotelImages/124/1246280/1246280_16061017110043391702.jpg",
      "https://cdn.britannica.com/96/115096-050-5AFDAF5D/Bellagio-Hotel-Casino-Las-Vegas.jpg",
      "https://bsmedia.business-standard.com/_media/bs/img/article/2021-02/05/full/1612508290-954.jpg",
      "https://www.epichotel.com/images/1700-960/one-bedroom-suite-05-c1d982d5.jpg",
    ],
    features: [
      "WiFi",
      "Swimming Pool",
      "Game Zone",
      "Movie Theatre",
      "24/7 Support",
    ],
  });
  const hotel3 = new Hotel({
    name: "Le Meridian",
    address: "Dhaka, Bangladesh",
    description: `
     Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
     Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,`,
    imageUrl:
      "https://pix10.agoda.net/hotelImages/5079276/0/113cbf78be1db3b289cdb5a1f3099a4b.jpg?s=1024x768",
    bookingPrice: 4500,
    allImages: [
      "https://images.pexels.com/photos/164595/pexels-photo-164595.jpeg",
      "https://pix10.agoda.net/hotelImages/124/1246280/1246280_16061017110043391702.jpg",
      "https://cdn.britannica.com/96/115096-050-5AFDAF5D/Bellagio-Hotel-Casino-Las-Vegas.jpg",
      "https://bsmedia.business-standard.com/_media/bs/img/article/2021-02/05/full/1612508290-954.jpg",
      "https://www.epichotel.com/images/1700-960/one-bedroom-suite-05-c1d982d5.jpg",
    ],
    features: [
      "WiFi",
      "Swimming Pool",
      "Game Zone",
      "Movie Theatre",
      "24/7 Support",
    ],
  });

  console.log("[INF] creating hotels");
  try {
    await hotel1.save();
    await hotel2.save();
    await hotel3.save();
    console.log("[INF] hotels created successfully!");
  } catch (err) {
    console.error("[ERR] failed to create hotels " + err);
  }
};

const seed = () => {
  seedDefaultAdmin();
  seedDefaultPlaces();
  seedDefaultHotels();
};

module.exports = {
  seed: seed,
};
