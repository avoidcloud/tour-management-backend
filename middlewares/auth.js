let jwt = require("jsonwebtoken");
let User = require("../models/user");
const { Role } = require("../util");

const SECRET_KEY = "1k1231-_()9sdWt_j11321";

let authentication = (req, res, next) => {
  let token = req.headers["x-access-token"] || req.headers["authorization"];
  if (token) {
    if (token.startsWith("Bearer ")) {
      token = token.slice(7, token.length);
      jwt.verify(token, SECRET_KEY, (err, decoded) => {
        if (err) {
          return res.send({
            authication: "failed",
            message: "invalid token",
          });
        }
        req.decoded = decoded;
        getUser(req, res, next);
      });
    } else {
      return res.send({
        message: "invalid auth token",
      });
    }
  } else {
    return res.send({
      message: "authentication required",
    });
  }
};

let generateAuthToken = (user) => {
  let token = jwt.sign({ id: user.id, role: user.role }, SECRET_KEY, {
    expiresIn: "24h",
  });
  return token;
};

let getUser = async (req, res, next) => {
  try {
    const user = await User.findById(req.decoded.id);

    req.user = user;
    next();
  } catch (err) {
    res.send("Error " + err);
  }
};

let admin = (req, res, next) => {
  const user = req.user;
  if (user.role !== Role.Admin) {
    return res.status(403).send({
      message: "access denied",
    });
  }
};

module.exports = {
  authentication: authentication,
  admin: admin,
  generateAuthToken: generateAuthToken,
};
